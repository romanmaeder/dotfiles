---?color=#E58537;

@snap[west span-60]
@code[uml rawcode zoom-15](src/uml/sequence.puml)
@snapend

@snap[east span-40]
@uml[span-100 bg-white](src/uml/sequence.puml)
@snapend

@snap[south-west span-100]
@[1,6](Start with opening and closing PlantUML delimiters.)
@[2,3](Declare participants, actors, boundaries, etc.)
@[4,5](Then define the messages between participants.).
@snapend