#!/bin/sh

echo "Setting up your Mac..."

# Check for Homebrew and install if we don't have it
if test ! $(which brew); then
  # /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  echo "install brew"
else
  echo "Homebrew is already installed."
fi

# Disable Homebrew analytics.
brew analytics off

# Update Homebrew recipes
brew update

# Install all our dependencies with bundle (See Brewfile)
brew tap homebrew/bundle
brew bundle

brew tap cloudfoundry/tap
brew install cf-cli
brew install https://ludocode.github.io/msgpack-tools.rb
brew install mosquitto
brew install lua luarocks
brew cask install mqttfx
brew cask install skype-for-business

echo 'export PATH="$HOME/.jenv/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(jenv init -)"' >> ~/.zshrc

# Ensure Maven uses the JDK activated with Jenv
jenv enable-plugin maven