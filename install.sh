#!/usr/bin/env bash

echo "Setting up your Mac..."
sudo -v

# Keep-alive: update existing sudo time stamp until the script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Check for Homebrew and install if we don't have it
if test ! $(which brew); then
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
  echo "Homebrew is already installed."
fi

# Disable Homebrew analytics.
brew analytics off

# Update Homebrew recipes
brew update

# Install all our dependencies with bundle (See Brewfile)
brew tap homebrew/bundle
brew bundle
# Remove outdated versions from the cellar.
brew cleanup

CURRENTSHELL=$(dscl . -read /Users/$USER UserShell | awk '{print $2}')
if [[ "$CURRENTSHELL" != "/usr/local/bin/zsh" ]]; then
  echo "Setting newer homebrew zsh (/usr/local/bin/zsh) as your shell (password required)"
  sudo bash -c 'echo "/usr/local/bin/zsh" >> /etc/shells'
  chsh -s /usr/local/bin/zsh
fi

git clone https://github.com/robbyrussell/oh-my-zsh.git $HOME/.oh-my-zsh
git clone https://github.com/bhilburn/powerlevel9k.git $HOME/.dotfiles/themes/powerlevel9k
git clone https://github.com/mbadolato/iTerm2-Color-Schemes.git $HOME/.dotfiles/iTerm2-Color-Schemes

# Removes .zshrc from $HOME (if it exists) and symlinks the .zshrc file from the .dotfiles
rm -rf $HOME/.zshrc
ln -s $HOME/.dotfiles/.zshrc $HOME/.zshrc

# Symlink the Mackup config file to the home directory
ln -s $HOME/.dotfiles/.mackup.cfg $HOME/.mackup.cfg

# Symlink the global .gitignore file to the home directory
ln -s $HOME/.dotfiles/.gitignore_global $HOME/.gitignore_global

# Ensure Maven uses the JDK activated with Jenv
jenv enable-plugin maven

# Set macOS preferences
# We will run this last because this will reload the shell
# source .macos
