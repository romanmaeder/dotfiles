#!/usr/bin/env bash

echo "[INFO] Installing your Mac..."

echo "[INFO] Checking for Homebrew..."
if test ! $(which brew); then
    echo "[INFO] Installing Homebrew..."
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    echo "[INFO] Homebrew installed."

    # Disable Homebrew analytics.
    brew analytics off
else
    echo "[INFO] Homebrew already installed."
fi

echo "[INFO] Installing Applications..."
brew tap homebrew/bundle
brew bundle --file=brewfile-arviem-default

# Remove outdated versions from the cellar.
brew cleanup
echo "[INFO] Finished installing your Mac."